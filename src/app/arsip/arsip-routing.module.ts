import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArsipPage } from './arsip.page';

const routes: Routes = [
  {
    path: '',
    component: ArsipPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArsipPageRoutingModule {}
