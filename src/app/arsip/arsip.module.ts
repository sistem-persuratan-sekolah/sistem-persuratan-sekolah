import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArsipPageRoutingModule } from './arsip-routing.module';

import { ArsipPage } from './arsip.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArsipPageRoutingModule
  ],
  declarations: [ArsipPage]
})
export class ArsipPageModule {}
