import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ArsipPage } from './arsip.page';

describe('ArsipPage', () => {
  let component: ArsipPage;
  let fixture: ComponentFixture<ArsipPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArsipPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ArsipPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
