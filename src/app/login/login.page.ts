import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public router: Router,
    public ModalController: ModalController,
    public auth: AngularFireAuth,
    public alertController: AlertController,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }
email:any;
password:any;
loading: boolean;
masuk(){
  this.auth
  .auth
  .signInWithEmailAndPassword(this.email, this.password)
  .then(value=>{

  
  this.router.navigate(['home']);
})
.catch(err => {
  this.presentAlert()
});
}
selectedSegment:any = 'login';
async presentAlert(){
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'ups!',
    message: 'password/email yang anda masukkan salah/tidak terdaftar!',
    buttons:['ok']
  });
  await alert.present();
}
async back(){
  const modal = await this.modalCtrl.create({
    component: '',
    cssClass: 'my-custom-class',
  });
  return await modal.present();
}
}
