import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Arsip4PageRoutingModule } from './arsip4-routing.module';

import { Arsip4Page } from './arsip4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Arsip4PageRoutingModule
  ],
  declarations: [Arsip4Page]
})
export class Arsip4PageModule {}
