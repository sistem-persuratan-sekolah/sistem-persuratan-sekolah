import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Arsip4Page } from './arsip4.page';

const routes: Routes = [
  {
    path: '',
    component: Arsip4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Arsip4PageRoutingModule {}
