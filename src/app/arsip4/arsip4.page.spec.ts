import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Arsip4Page } from './arsip4.page';

describe('Arsip4Page', () => {
  let component: Arsip4Page;
  let fixture: ComponentFixture<Arsip4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Arsip4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Arsip4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
