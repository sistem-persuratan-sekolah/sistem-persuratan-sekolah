import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Disposisi2Page } from './disposisi2.page';

const routes: Routes = [
  {
    path: '',
    component: Disposisi2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Disposisi2PageRoutingModule {}
