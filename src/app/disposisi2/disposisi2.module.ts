import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Disposisi2PageRoutingModule } from './disposisi2-routing.module';

import { Disposisi2Page } from './disposisi2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Disposisi2PageRoutingModule
  ],
  declarations: [Disposisi2Page]
})
export class Disposisi2PageModule {}
