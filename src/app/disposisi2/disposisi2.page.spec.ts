import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Disposisi2Page } from './disposisi2.page';

describe('Disposisi2Page', () => {
  let component: Disposisi2Page;
  let fixture: ComponentFixture<Disposisi2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Disposisi2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Disposisi2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
