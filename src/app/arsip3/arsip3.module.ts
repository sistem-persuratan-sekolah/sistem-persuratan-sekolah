import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Arsip3PageRoutingModule } from './arsip3-routing.module';

import { Arsip3Page } from './arsip3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Arsip3PageRoutingModule
  ],
  declarations: [Arsip3Page]
})
export class Arsip3PageModule {}
