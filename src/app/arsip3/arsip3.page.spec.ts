import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Arsip3Page } from './arsip3.page';

describe('Arsip3Page', () => {
  let component: Arsip3Page;
  let fixture: ComponentFixture<Arsip3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Arsip3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Arsip3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
