import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Arsip3Page } from './arsip3.page';

const routes: Routes = [
  {
    path: '',
    component: Arsip3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Arsip3PageRoutingModule {}
