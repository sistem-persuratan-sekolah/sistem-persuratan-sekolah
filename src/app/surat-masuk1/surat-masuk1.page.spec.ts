import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratMasuk1Page } from './surat-masuk1.page';

describe('SuratMasuk1Page', () => {
  let component: SuratMasuk1Page;
  let fixture: ComponentFixture<SuratMasuk1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratMasuk1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratMasuk1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
