import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuratMasuk1PageRoutingModule } from './surat-masuk1-routing.module';

import { SuratMasuk1Page } from './surat-masuk1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuratMasuk1PageRoutingModule
  ],
  declarations: [SuratMasuk1Page]
})
export class SuratMasuk1PageModule {}
