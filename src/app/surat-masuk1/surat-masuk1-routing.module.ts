import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuratMasuk1Page } from './surat-masuk1.page';

const routes: Routes = [
  {
    path: '',
    component: SuratMasuk1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuratMasuk1PageRoutingModule {}
