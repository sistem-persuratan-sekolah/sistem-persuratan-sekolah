import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  
  
  
  
  
  
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'arsip',
    loadChildren: () => import('./arsip/arsip.module').then( m => m.ArsipPageModule)
  },
  {
    path: 'arsip2',
    loadChildren: () => import('./arsip2/arsip2.module').then( m => m.Arsip2PageModule)
  },
  {
    path: 'arsip3',
    loadChildren: () => import('./arsip3/arsip3.module').then( m => m.Arsip3PageModule)
  },
  {
    path: 'arsip4',
    loadChildren: () => import('./arsip4/arsip4.module').then( m => m.Arsip4PageModule)
  },
  {
    path: 'arsip5',
    loadChildren: () => import('./arsip5/arsip5.module').then( m => m.Arsip5PageModule)
  },
  {
    path: 'disposisi1',
    loadChildren: () => import('./disposisi1/disposisi1.module').then( m => m.Disposisi1PageModule)
  },
  {
    path: 'disposisi2',
    loadChildren: () => import('./disposisi2/disposisi2.module').then( m => m.Disposisi2PageModule)
  },
  {
    path: 'agenda2',
    loadChildren: () => import('./agenda2/agenda2.module').then( m => m.Agenda2PageModule)
  },
  {
    path: 'agenda',
    loadChildren: () => import('./agenda/agenda.module').then( m => m.AgendaPageModule)
  },
  {
    path: 'agenda1',
    loadChildren: () => import('./agenda1/agenda1.module').then( m => m.Agenda1PageModule)
  },
  {
    path: 'surat-masuk',
    loadChildren: () => import('./surat-masuk/surat-masuk.module').then( m => m.SuratMasukPageModule)
  },
  {
    path: 'surat-masuk1',
    loadChildren: () => import('./surat-masuk1/surat-masuk1.module').then( m => m.SuratMasuk1PageModule)
  },
  {
    path: 'scan',
    loadChildren: () => import('./scan/scan.module').then( m => m.ScanPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
