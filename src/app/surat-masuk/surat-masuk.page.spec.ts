import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuratMasukPage } from './surat-masuk.page';

describe('SuratMasukPage', () => {
  let component: SuratMasukPage;
  let fixture: ComponentFixture<SuratMasukPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuratMasukPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuratMasukPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
