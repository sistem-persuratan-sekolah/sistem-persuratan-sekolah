import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Disposisi1Page } from './disposisi1.page';

describe('Disposisi1Page', () => {
  let component: Disposisi1Page;
  let fixture: ComponentFixture<Disposisi1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Disposisi1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Disposisi1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
