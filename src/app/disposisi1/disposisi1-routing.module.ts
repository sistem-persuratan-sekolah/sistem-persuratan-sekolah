import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Disposisi1Page } from './disposisi1.page';

const routes: Routes = [
  {
    path: '',
    component: Disposisi1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Disposisi1PageRoutingModule {}
