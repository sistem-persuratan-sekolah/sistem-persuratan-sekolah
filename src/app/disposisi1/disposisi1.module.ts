import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Disposisi1PageRoutingModule } from './disposisi1-routing.module';

import { Disposisi1Page } from './disposisi1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Disposisi1PageRoutingModule
  ],
  declarations: [Disposisi1Page]
})
export class Disposisi1PageModule {}
