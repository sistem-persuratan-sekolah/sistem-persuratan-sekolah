import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Arsip2Page } from './arsip2.page';

describe('Arsip2Page', () => {
  let component: Arsip2Page;
  let fixture: ComponentFixture<Arsip2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Arsip2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Arsip2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
