import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Arsip2Page } from './arsip2.page';

const routes: Routes = [
  {
    path: '',
    component: Arsip2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Arsip2PageRoutingModule {}
