import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Arsip2PageRoutingModule } from './arsip2-routing.module';

import { Arsip2Page } from './arsip2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Arsip2PageRoutingModule
  ],
  declarations: [Arsip2Page]
})
export class Arsip2PageModule {}
