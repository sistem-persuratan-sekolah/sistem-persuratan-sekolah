import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Arsip5PageRoutingModule } from './arsip5-routing.module';

import { Arsip5Page } from './arsip5.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Arsip5PageRoutingModule
  ],
  declarations: [Arsip5Page]
})
export class Arsip5PageModule {}
