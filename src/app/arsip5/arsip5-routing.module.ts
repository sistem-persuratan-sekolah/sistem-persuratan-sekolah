import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Arsip5Page } from './arsip5.page';

const routes: Routes = [
  {
    path: '',
    component: Arsip5Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Arsip5PageRoutingModule {}
