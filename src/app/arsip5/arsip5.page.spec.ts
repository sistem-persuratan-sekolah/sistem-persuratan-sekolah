import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Arsip5Page } from './arsip5.page';

describe('Arsip5Page', () => {
  let component: Arsip5Page;
  let fixture: ComponentFixture<Arsip5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Arsip5Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Arsip5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
