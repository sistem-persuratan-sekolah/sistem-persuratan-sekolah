import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Agenda1Page } from './agenda1.page';

describe('Agenda1Page', () => {
  let component: Agenda1Page;
  let fixture: ComponentFixture<Agenda1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Agenda1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Agenda1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
