import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Agenda1PageRoutingModule } from './agenda1-routing.module';

import { Agenda1Page } from './agenda1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Agenda1PageRoutingModule
  ],
  declarations: [Agenda1Page]
})
export class Agenda1PageModule {}
