import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Agenda1Page } from './agenda1.page';

const routes: Routes = [
  {
    path: '',
    component: Agenda1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Agenda1PageRoutingModule {}
