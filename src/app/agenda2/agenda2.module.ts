import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Agenda2PageRoutingModule } from './agenda2-routing.module';

import { Agenda2Page } from './agenda2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Agenda2PageRoutingModule
  ],
  declarations: [Agenda2Page]
})
export class Agenda2PageModule {}
