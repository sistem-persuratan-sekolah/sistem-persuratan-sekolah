import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Agenda2Page } from './agenda2.page';

const routes: Routes = [
  {
    path: '',
    component: Agenda2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Agenda2PageRoutingModule {}
