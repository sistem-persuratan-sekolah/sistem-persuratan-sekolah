import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Agenda2Page } from './agenda2.page';

describe('Agenda2Page', () => {
  let component: Agenda2Page;
  let fixture: ComponentFixture<Agenda2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Agenda2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Agenda2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
